﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AStar : MonoBehaviour
{
    [SerializeField] Color highlightColor = Color.yellow;   // The color of the tile in the path
    public List<Tile> path = new List<Tile>();
    public Tile destinationTile;

    List<Tile> openList = new List<Tile>();
    List<Tile> closedList = new List<Tile>();
    List<Tile> obstacles = new List<Tile>();
    bool stillSolving = true;

    /// <summary>
    /// Set tiles' parent and child to null and clear openlist
    /// </summary>
    void ClearOpenList()
    {
        foreach (Tile tile in openList)
        {
            tile.ParentTile = null;
            tile.ChildTile = null;
        }
        openList.Clear();
    }

    /// <summary>
    /// Set tiles' parent and child to null and clear closedlist
    /// </summary>
    void ClearClosedList()
    {
        foreach (Tile tile in closedList)
        {
            tile.ParentTile = null;
            tile.ChildTile = null;
        }
        closedList.Clear();
    }

    /// <summary>
    /// Resets each tile's parent, child and f,g,h values
    /// </summary>
    void ClearPath()
    {
        foreach(Tile tile in path)
        {
            tile.ResetTile();
        }
        path.Clear();
    }

    /// <summary>
    /// Un-highlight tiles and clear open and closed lists.
    /// Clears current path and sets "solving" bool to true.
    /// </summary>
    private void ResetAStar()
    {
        foreach(Tile tile in path)
        {
            tile.DeselectTile();
            tile.obstacle = false;
        }

        stillSolving = true;
        ClearOpenList();
        ClearClosedList();
        ClearPath();
    }

    public void StartAStar(Tile _startTile)
    {
        ResetAStar();
        openList.Add(_startTile);
        DoPathfinding(_startTile);
        while (stillSolving)
        {
            Tile temp = null;
            
            while (openList.Count > 0 && stillSolving)
            {
                if (temp == null)
                {
                    temp = openList[0];
                }
                else
                {
                    if (temp.FValue > openList[0].FValue)
                        temp = openList[0];
                }
                DoPathfinding(openList[0]);
            }

            if (openList.Count <= 0)
                stillSolving = false;
        }
        ColourPath(destinationTile);
        GeneratePathList(destinationTile, ref path);
    }

    /// <summary>
    /// Check _tile's adjacent tiles adding non-obstacles to open list and calculating their scores.
    /// </summary>
    /// <param name="_tile"></param>
    /// <returns></returns>
    void DoPathfinding(Tile _tile)
    {
        // To prevent checking the same _tile multiple times
        closedList.Add(_tile);
        openList.Remove(_tile);

        foreach (Tile tile in _tile.AdjacentTiles)
        {
            bool inOpenList = false;
            bool inClosedList = false;
            Tile temp = IsSpaceOccupied(tile, ref inOpenList, ref inClosedList);
            if (tile.obstacle == true)
            {
                // Set to true to prevent any further action.
                inOpenList = true;
                inClosedList = true;
            }

            if (!inOpenList)
            {
                _tile.ChildTile = tile;
                tile.ParentTile = _tile;
                tile.CalculateF(destinationTile);
                openList.Add(tile);
                stillSolving = CheckEndTile(tile);
            }
            else
            {
                if (tile.obstacle == false)
                {
                    if (!inClosedList)
                        temp.CalculateF(destinationTile);
                }
            }

            if (!stillSolving)
                break;
        }
    }

    /// <summary>
    /// Check if _tile is already in open list or closed list
    /// </summary>
    /// <param name="_tile"></param>
    /// <param name="_inOpenList"></param>
    /// <param name="_inClosedList"></param>
    /// <returns></returns>
    Tile IsSpaceOccupied(Tile _tile, ref bool _inOpenList, ref bool _inClosedList)
    {
        foreach (Tile _listTile in openList)
        {
            if (_tile == _listTile)
            {
                _inOpenList = true;
                return _listTile;
            }
        }

        foreach (Tile _listTile in closedList)
        {
            if (_tile == _listTile)
            {
                _inOpenList = true;
                _inClosedList = true;
            }
        }

        return null;
    }

    /// <summary>
    /// Returns if the _childTile is the destination tile
    /// </summary>
    /// <param name="_childTile"></param>
    /// <returns></returns>
    bool CheckEndTile(Tile _childTile)
    {
        if (_childTile == destinationTile)
        {
            destinationTile.ParentTile = _childTile.ParentTile;
            return false;
        }
        return true;
    }

    /// <summary>
    /// Color each tile in the path the highlightColor
    /// </summary>
    /// <param name="_tile"></param>
    void ColourPath(Tile _tile)
    {
        if (_tile.ParentTile == null)
            return;
        else
            ColourPath(_tile.ParentTile);

        _tile.SelectTile(highlightColor);
    }

    /// <summary>
    /// Adds all tiles in the path to a list
    /// </summary>
    /// <param name="_tile"></param>
    /// <param name="_path"></param>
    void GeneratePathList(Tile _tile, ref List<Tile> _path)
    {
        if (_tile.ParentTile == null)
        {
            return;
        }
        else
        {
            GeneratePathList(_tile.ParentTile, ref _path);
        }
        _path.Add(_tile);
    }
}
