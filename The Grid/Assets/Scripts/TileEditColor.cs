﻿using UnityEngine;

[ExecuteInEditMode]
public class TileEditColor : MonoBehaviour
{
#if UNITY_EDITOR
    Tile tileScript;
    MeshRenderer renderer;

    // Use this for initialization
    void Start()
    {
        tileScript = GetComponent<Tile>();
        renderer = GetComponent<MeshRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        if (UnityEditor.EditorApplication.isPlaying)
            return;

        if (tileScript != null)
        {
            if (renderer != null)
            {
                if (tileScript.obstacle)
                {
                    var tempMaterial = new Material(renderer.sharedMaterial);
                    tempMaterial.color = Color.red;
                    renderer.sharedMaterial = tempMaterial;
                }
                else
                {
                    var tempMaterial = new Material(renderer.sharedMaterial);
                    tempMaterial.color = Color.white;
                    renderer.sharedMaterial = tempMaterial;
                }
            }
        }
    }
#endif
}
