﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    [SerializeField] float moveSpeed = 10.0f;
    [SerializeField] float rotateSpeed = 50.0f;
    [SerializeField] float scrollSpeed = 2.0f;
    [SerializeField] GameObject pivot;
    [SerializeField] float xMin = 0.0f;
    [SerializeField] float xMax = 10.0f;
    [SerializeField] float zMin = 0.0f;
    [SerializeField] float zMax = 8.0f;
    [SerializeField] float zoomInMax = -5.0f;
    [SerializeField] float zoomOutMax = -20.0f;


    // Update is called once per frame
    void Update()
    {
        CheckBoundaries();
        transform.position += transform.forward * Input.GetAxis("Mouse ScrollWheel") * scrollSpeed;
        pivot.transform.position += ((Input.GetAxisRaw("Vertical") * pivot.transform.forward) + (Input.GetAxisRaw("Horizontal") * pivot.transform.right)) * moveSpeed * Time.deltaTime;
        pivot.transform.Rotate(Vector3.up, Input.GetAxisRaw("RotateAxis") * rotateSpeed * Time.deltaTime);
    }

    void CheckBoundaries()
    {
        bool moving = Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0;
        bool rotating = Input.GetAxis("RotateAxis") != 0;

        if (moving && !rotating)
        {
            if (pivot.transform.position.x < xMin)
            {
                Vector3 temp = pivot.transform.position;
                temp.x = xMin;
                pivot.transform.position = temp;
            }
            else if (pivot.transform.position.x > xMax)
            {
                Vector3 temp = pivot.transform.position;
                temp.x = xMax;
                pivot.transform.position = temp;
            }
            if (pivot.transform.position.z < zMin)
            {
                Vector3 temp = pivot.transform.position;
                temp.z = zMin;
                pivot.transform.position = temp;
            }
            else if (pivot.transform.position.z > zMax)
            {
                Vector3 temp = pivot.transform.position;
                temp.z = zMax;
                pivot.transform.position = temp;
            }

            if (transform.position.z > zoomInMax)
            {
                Vector3 temp = transform.position;
                temp.z = zoomInMax;
                transform.position = temp;
            }
            if (transform.position.z < zoomOutMax)
            {
                Vector3 temp = transform.position;
                temp.z = zoomOutMax;
                transform.position = temp;
            }
        }
    }
}
