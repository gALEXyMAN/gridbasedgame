﻿using UnityEngine;

public class TileContainer : MonoBehaviour
{
    private static TileContainer _instance;
    public static TileContainer Instance
    {
        get
        {
            if (_instance == null)
            {
#if UNITY_EDITOR
                GameObject o;
                if (Application.isPlaying)
                {
                    o = (GameObject)Instantiate(Resources.Load("TileContainer"));
                }
                else
                {
                    o = UnityEditor.AssetDatabase.LoadAssetAtPath<GameObject>("Assets/Resources.TileContainer.prefab");
                    if (o == null)
                    {
                        GameObject tempGO = new GameObject("TileContainer", typeof(TileContainer));
                        o = UnityEditor.PrefabUtility.CreatePrefab("Assets/Resources.TileContainer.prefab", tempGO);
                        DestroyImmediate(tempGO);
                    }
                }
#else
                    Gameobject o = (Gameobject)Instantiate(Resources.Load("ResourcesSingleton"));
#endif
                _instance = ((GameObject)o).GetComponent<TileContainer>();
            }
            return _instance;

        }
    }

    private void Awake()
    {
        if (_instance == null)
        {
            _instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else if(_instance != this)
        {
            Debug.LogWarning(_instance.name + " already exists! Destroying " + this.name);
            Destroy(gameObject);
        }
    }

    private void OnDestroy()
    {
        if (_instance == this)
            _instance = null;
    }

    [SerializeField] GameObject tileOne;
    [SerializeField] GameObject tileTwo;
    [SerializeField] GameObject rampOne;
    [SerializeField] GameObject rampTwo;
    [SerializeField] GameObject steepRampOne;
    [SerializeField] GameObject steepRampTwo;
    [SerializeField] GameObject cornerOne;
    [SerializeField] GameObject cornerTwo;
    [SerializeField] GameObject steepCornerOne;
    [SerializeField] GameObject steepCornerTwo;
    [SerializeField] GameObject halfWall;
    [SerializeField] GameObject wall;
    [SerializeField] GameObject edgeHalfWall;
    [SerializeField] GameObject edgeWall;

    public GameObject TileOne { get { return tileOne; } }
    public GameObject TileTwo { get { return tileTwo; } }
    public GameObject RampOne { get { return rampOne; } }
    public GameObject RampTwo { get { return rampTwo; } }
    public GameObject SteepRampOne { get { return steepRampOne; } }
    public GameObject SteepRampTwo { get { return steepRampTwo; } }
    public GameObject CornerOne { get { return cornerOne; } }
    public GameObject CornerTwo { get { return cornerTwo; } }
    public GameObject SteepCornerOne { get { return steepCornerOne; } }
    public GameObject SteepCornerTwo { get { return steepCornerTwo; } }
    public GameObject HalfWall { get { return halfWall; } }
    public GameObject Wall { get { return wall; } }
    public GameObject EdgeHalfWall { get { return edgeHalfWall; } }
    public GameObject EdgeWall { get { return edgeWall; } }
}
