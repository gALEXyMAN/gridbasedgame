﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour
{
    [SerializeField] List<Tile> adjacentTiles = new List<Tile>();
    public bool obstacle = false;
    Color defaultColor;
    Tile parentTile = null, childTile = null;
    int F = 0, G = 0, H = 0;

    public List<Tile> AdjacentTiles { get { return adjacentTiles; } }
    public Tile ChildTile { set { childTile = value; } }
    public Tile ParentTile { get { return parentTile; } set { parentTile = value; } }
    public int FValue { get { return F; } }

    private void Start()
    {
        defaultColor = GetComponent<MeshRenderer>().material.color;
    }
    
    /// <summary>
    /// Calculate F score from current tile to _destinationTile
    /// </summary>
    /// <param name="_destinationTile"></param>
    public void CalculateF(Tile _destinationTile)
    {
        int f = 0, g = 0, h = 0;
        float x = transform.position.x;
        float z = transform.position.z;

        if (parentTile != null)
        {
            if (transform.position.x == parentTile.transform.position.x ||
                transform.position.z == parentTile.transform.position.z)
            {
                g = parentTile.G + 10;
            }
            else
            {
                // Diagonally adjacent
                g = parentTile.G + 14;
            }
        }

        while (x > _destinationTile.transform.position.x)
        {
            h += 10;
            x--;
        }

        while (x < _destinationTile.transform.position.x)
        {
            h += 10;
            x++;
        }

        while (z > _destinationTile.transform.position.z)
        {
            h += 10;
            z--;
        }

        while (z < _destinationTile.transform.position.z)
        {
            h += 10;
            z++;
        }

        f = g + h;
        G = g;
        F = f;
    }

    /// <summary>
    /// Change material color to specified _highlightColor
    /// </summary>
    /// <param name="_highlightColor"></param>
    public void SelectTile(Color _highlightColor)
    {
        GetComponent<MeshRenderer>().material.color = _highlightColor;
    }

    /// <summary>
    /// Return material color to starting color
    /// </summary>
    public void DeselectTile()
    {
        GetComponent<MeshRenderer>().material.color = defaultColor;
    }

    /// <summary>
    /// Sets parent and child to null and f,g,h to 0
    /// </summary>
    public void ResetTile()
    {
        parentTile = null;
        childTile = null;
        F = 0;
        G = 0;
        H = 0;
    }
}
