﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] float speed = 1.0f;
    [SerializeField] Color highlightColor = Color.red;
    [SerializeField] Tile currentTile;
    [SerializeField] bool currentlySelected = false;
    public bool moving = false;
    int pathIndex = 0;
    AStar pathfinder;
    Tile lastSelectedTile;
    GameObject[] players;

    // Use this for initialization
    void Start()
    {
        players = GameObject.FindGameObjectsWithTag("Player");
        pathfinder = GetComponent<AStar>();
        if (currentTile == null)
        {
            foreach(Tile tile in FindObjectsOfType<Tile>())
            {
                if (tile.transform.position.x == transform.position.x - 0.5f &&
                    tile.transform.position.z == transform.position.z - 0.5f)
                {
                    currentTile = tile;
                }
            }
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.transform.gameObject == this.gameObject)
                {
                    SelectPlayer();
                }
                else if (hit.transform.gameObject.tag == "Tile")
                {
                    SelectTile(hit);
                }
            }
        }
        Movement();
    }

    /// <summary>
    /// Start A* pathfinder towards clicked tile from current tile.
    /// </summary>
    /// <param name="_hit"></param>
    void SelectTile(RaycastHit _hit)
    {
        if (currentlySelected)
        {
            if (lastSelectedTile != null)
            {
                lastSelectedTile.DeselectTile();
                lastSelectedTile.obstacle = false;
            }
            lastSelectedTile = _hit.transform.GetComponent<Tile>();

            // Prevent clicking on tile that I'm already on
            if (lastSelectedTile != currentTile)
            {
                pathIndex = 0;
                pathfinder.destinationTile = lastSelectedTile;
                pathfinder.StartAStar(currentTile);
                _hit.transform.GetComponent<Tile>().SelectTile(highlightColor);
                moving = true;

                RotateTowardsTile();
            }
        }
    }

    /// <summary>
    /// Set all other players 'selected' bool to false and inverse current players 'selected' bool
    /// </summary>
    void SelectPlayer()
    {
        foreach (GameObject player in players)
        {
            if (player != this.gameObject)
                player.GetComponent<PlayerController>().currentlySelected = false;
        }
        currentlySelected = !currentlySelected;
    }

    /// <summary>
    /// Move towards next adjacent tile for each tile in the path
    /// </summary>
    void Movement()
    {
        if (moving)
        {
            if (pathfinder.path.Count > 0)
            {
                currentTile = pathfinder.path[pathIndex];
                currentTile.DeselectTile();
                currentTile.obstacle = true;
                bool hasChild = currentTile.transform.childCount > 0;
                Vector3 destination;
                if (hasChild)
                {
                    destination = currentTile.transform.GetChild(0).transform.position;
                    destination.y += 1.0f;
                }
                else
                {
                    destination = currentTile.transform.position;
                    destination.y += 1.5f;
                }
                StartCoroutine(IncreaseXPos(destination.x));
                StartCoroutine(DecreaseXPos(destination.x));
                StartCoroutine(IncreaseZPos(destination.z));
                StartCoroutine(DecreaseZPos(destination.z));
                StartCoroutine(IncreaseYPos(destination.y));
                StartCoroutine(DecreaseYPos(destination.y));

                if (hasChild)
                {
                    if (transform.position.x == currentTile.transform.GetChild(0).transform.position.x &&
                    transform.position.z == currentTile.transform.GetChild(0).transform.position.z)
                    {
                        SetNextTile();
                    }
                }
                else
                {
                    if (transform.position.x == currentTile.transform.position.x &&
                        transform.position.z == currentTile.transform.position.z)
                    {
                        SetNextTile();
                    }
                }
            }
            else
            {
                Debug.Log("Path is empty...");
            }
        }
    }

    /// <summary>
    /// Set next tile to move to unless it's the last tile
    /// </summary>
    void SetNextTile()
    {
        if (pathIndex < pathfinder.path.Count - 1)
        {
            pathIndex++;
            currentTile.obstacle = false;
            RotateTowardsTile();
        }
        else
        {
            lastSelectedTile.DeselectTile();
            pathIndex = 0;
            moving = false;
        }
    }

    /// <summary>
    /// Rotate transform to face the next tile in the path
    /// </summary>
    void RotateTowardsTile()
    {
        Vector3 target;
        if (pathfinder.path[pathIndex].transform.childCount > 0)
        {
            target = pathfinder.path[pathIndex].transform.GetChild(0).transform.position;
        }
        else
        {
            target = pathfinder.path[pathIndex].transform.position;
        }

        Vector3 forward = target - transform.position;
        forward.y = 0.0f;
        transform.forward = forward;
    }

    IEnumerator IncreaseXPos(float _destinationX)
    {
        while (transform.position.x < _destinationX)
        {
            Vector3 temp = transform.position;
            temp.x += speed * Time.deltaTime;
            transform.position = temp;

            if (transform.position.x > _destinationX)
            {
                temp = transform.position;
                temp.x = _destinationX;
                transform.position = temp;
            }
            yield return null;
        }
    }

    IEnumerator DecreaseXPos(float _destinationX)
    {
        while (transform.position.x > _destinationX)
        {
            Vector3 temp = transform.position;
            temp.x -= speed * Time.deltaTime;
            transform.position = temp;

            if (transform.position.x < _destinationX)
            {
                temp = transform.position;
                temp.x = _destinationX;
                transform.position = temp;
            }
            yield return null;
        }
    }

    IEnumerator IncreaseZPos(float _destinationZ)
    {
        while (transform.position.z < _destinationZ)
        {
            Vector3 temp = transform.position;
            temp.z += speed * Time.deltaTime;
            transform.position = temp;

            if (transform.position.z > _destinationZ)
            {
                temp = transform.position;
                temp.z = _destinationZ;
                transform.position = temp;
            }
            yield return null;
        }
    }

    IEnumerator DecreaseZPos(float _destinationZ)
    {
        while (transform.position.z > _destinationZ)
        {
            Vector3 temp = transform.position;
            temp.z -= speed * Time.deltaTime;
            transform.position = temp;

            if (transform.position.z < _destinationZ)
            {
                temp = transform.position;
                temp.z = _destinationZ;
                transform.position = temp;
            }
            yield return null;
        }
    }

    IEnumerator IncreaseYPos(float _destinationY)
    {
        while (transform.position.y < _destinationY)
        {
            Vector3 temp = transform.position;
            temp.y += speed * Time.deltaTime;
            transform.position = temp;

            if (transform.position.y > _destinationY)
            {
                temp = transform.position;
                temp.y = _destinationY;
                transform.position = temp;
            }
            yield return null;
        }
    }

    IEnumerator DecreaseYPos(float _destinationY)
    {
        while (transform.position.y > _destinationY)
        {
            Vector3 temp = transform.position;
            temp.y -= speed * Time.deltaTime;
            transform.position = temp;

            if (transform.position.y < _destinationY)
            {
                temp = transform.position;
                temp.y = _destinationY;
                transform.position = temp;
            }
            yield return null;
        }
    }
}
