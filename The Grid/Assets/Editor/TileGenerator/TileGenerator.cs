﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TileGenerator : EditorWindow
{
    Vector3 scale = new Vector3(1.0f, 1.0f, 1.0f);
    int rows;
    int columns;

    [MenuItem("Window/Tile Generator")]
    public static void ShowWindow()
    {
        GetWindow<TileGenerator>("Tile Generator");
    }

    private void OnGUI()
    {
        scale = EditorGUILayout.Vector3Field(new GUIContent("Scale", "The scale of each tile"), scale);
        rows = EditorGUILayout.IntField(new GUIContent("Rows", "Number of rows along z-axis"), rows);
        columns = EditorGUILayout.IntField(new GUIContent("Columns", "Number of columns along x-axis"), columns);

        if (GUILayout.Button("Generate"))
        {
            GenerateTiles();
        }
    }

    void GenerateTiles()
    {
        List<GameObject> children = new List<GameObject>();
        GameObject parentObject = new GameObject();
        parentObject.name = "Tiles";
        int tileIndex = 0;

        for (float i = 0; i < rows * scale.z; i += scale.z)
        {
            for (float j = 0; j < columns * scale.x; j += scale.x)
            {
                //GameObject temp = GameObject.CreatePrimitive(PrimitiveType.Cube);
                GameObject temp = Instantiate(TileContainer.Instance.TileOne);
                temp.name = "Tile " + tileIndex;
                temp.tag = "Tile";
                tileIndex++;

                Vector3 pos = Vector3.zero;
                pos.x = j;
                pos.z = i;

                temp.AddComponent<TileEditColor>();

                temp.transform.position = pos;
                temp.transform.localScale = scale;
                temp.transform.SetParent(parentObject.transform);

                children.Add(temp);
            }
        }

        SetAdjacentTiles(children);
    }

    void SetAdjacentTiles(List<GameObject> children)
    {
        for (int i = 0; i < children.Count; ++i)
        {
            bool top = i >= children.Count - columns;
            bool bottom = i < columns;
            bool leftSide = i % columns == 0 ? true : false;
            bool rightSide = (i % columns) + 1 == columns ? true : false;

            if (!bottom)
            {
                //if (!leftSide)
                //{
                //    children[i].GetComponent<Tile>().AdjacentTiles.Add(children[i - columns - 1].GetComponent<Tile>());
                //}
                children[i].GetComponent<Tile>().AdjacentTiles.Add(children[i - columns].GetComponent<Tile>());
                //if (!rightSide)
                //{
                //    children[i].GetComponent<Tile>().AdjacentTiles.Add(children[i - columns + 1].GetComponent<Tile>());
                //}
            }
            if (!leftSide)
            {
                children[i].GetComponent<Tile>().AdjacentTiles.Add(children[i - 1].GetComponent<Tile>());
            }
            if (!rightSide)
            {
                children[i].GetComponent<Tile>().AdjacentTiles.Add(children[i + 1].GetComponent<Tile>());
            }
            if (!top)
            {
                //if (!leftSide)
                //{
                //    children[i].GetComponent<Tile>().AdjacentTiles.Add(children[i + columns - 1].GetComponent<Tile>());
                //}
                children[i].GetComponent<Tile>().AdjacentTiles.Add(children[i + columns].GetComponent<Tile>());
                //if (!rightSide)
                //{
                //    children[i].GetComponent<Tile>().AdjacentTiles.Add(children[i + columns + 1].GetComponent<Tile>());
                //}
            }
        }
    }
}
