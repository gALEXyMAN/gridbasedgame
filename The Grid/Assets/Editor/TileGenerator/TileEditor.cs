﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TileEditor : EditorWindow
{
    enum TILETYPE
    {
        TILE = 0,
        RAMP,
        STEEP_RAMP,
        CORNER,
        STEEP_CORNER
    };

    enum TILEHEIGHT
    {
        ONE = 1,
        TWO
    };

    enum WALLTYPE
    {
        NONE = 0,
        HALF_WALL,
        WALL,
        EDGE_HALF_WALL,
        EDGE_WALL
    };

    enum WALLPOSITION
    {
        ZPOSITIVE = 0,
        ZNEGATIVE,
        XPOSITIVE,
        XNEGATIVE
    };

    TILETYPE tileType;
    TILEHEIGHT tileHeight;
    WALLTYPE wallType;
    WALLPOSITION wallPosition;

    [MenuItem("Window/Tile Editor")]
    public static void ShowWindow()
    {
        GetWindow<TileEditor>("Tile Editor");
    }

    private void OnGUI()
    {
        EditorGUILayout.LabelField("Tile", EditorStyles.boldLabel);
        tileType = (TILETYPE)EditorGUILayout.EnumPopup("Tile Type:", tileType);
        tileHeight = (TILEHEIGHT)EditorGUILayout.EnumPopup("Tile Height: ", tileHeight);
        if (GUILayout.Button("Change Selected"))
        {
            ChangeTile();
        }
        EditorGUILayout.LabelField("Wall", EditorStyles.boldLabel);
        wallType = (WALLTYPE)EditorGUILayout.EnumPopup("Wall Type:", wallType);
        wallPosition = (WALLPOSITION)EditorGUILayout.EnumPopup("Wall Position: ", wallPosition);
        if (GUILayout.Button("Add/Remove Wall"))
        {
            AddWall();
        }
        EditorGUILayout.LabelField("Link Tiles", EditorStyles.boldLabel);
        if (GUILayout.Button("Link Selected Tiles"))
        {
            LinkTiles();
        }
        EditorGUILayout.LabelField("Rotate Tile", EditorStyles.boldLabel);
        if (GUILayout.Button("ClockWise"))
        {
            RotateClockWise();
        }
        if (GUILayout.Button("Counter-ClockWise"))
        {
            RotateCounterClockWise();
        }
    }

    void ChangeTile()
    {
        var selectedObjects = Selection.gameObjects;

        for (int i = 0; i < selectedObjects.Length; ++i)
        {
            var selectedObject = selectedObjects[i];
            GameObject prefab = GetSpecifiedPrefab();
            var prefabType = PrefabUtility.GetPrefabType(prefab);

            GameObject newObject;

            if (prefabType == PrefabType.Prefab)
            {
                newObject = (GameObject)PrefabUtility.InstantiatePrefab(prefab);
            }
            else
            {
                newObject = Instantiate(prefab);
                newObject.name = selectedObject.name;
            }

            if (newObject == null)
            {
                Debug.LogError("Error instantiating prefab");
                break;
            }

            Undo.RegisterCreatedObjectUndo(newObject, "Replace with Prefabs");
            newObject.name = selectedObject.name;
            newObject.transform.parent = selectedObject.transform.parent;
            newObject.transform.localPosition = selectedObject.transform.localPosition;
            newObject.transform.localRotation = selectedObject.transform.localRotation;
            newObject.transform.localScale = selectedObject.transform.localScale;
            newObject.transform.SetSiblingIndex(selectedObject.transform.GetSiblingIndex());
            DestroyImmediate(newObject.GetComponent<Tile>());
            Tile tile = selectedObject.GetComponent<Tile>();
            UnityEditorInternal.ComponentUtility.CopyComponent(tile);
            UnityEditorInternal.ComponentUtility.PasteComponentAsNew(newObject);
            Undo.DestroyObjectImmediate(selectedObject);

            List<Tile> tiles = newObject.GetComponent<Tile>().AdjacentTiles;

            foreach (Tile _tile in newObject.GetComponent<Tile>().AdjacentTiles)
            {
                for (int index = 0; index < _tile.AdjacentTiles.Count; ++index)
                {
                    if (_tile.AdjacentTiles[index] == null)
                    {
                        _tile.AdjacentTiles[index] = newObject.GetComponent<Tile>();
                    }
                }
            }
        }
    }

    GameObject GetSpecifiedPrefab()
    {
        GameObject prefabType = TileContainer.Instance.TileOne;
        switch (tileType)
        {
            case TILETYPE.TILE:
                switch (tileHeight)
                {
                    case TILEHEIGHT.ONE:
                        prefabType = TileContainer.Instance.TileOne;
                        break;
                    case TILEHEIGHT.TWO:
                        prefabType = TileContainer.Instance.TileTwo;
                        break;
                    default:
                        break;
                }
                break;
            case TILETYPE.RAMP:
                switch (tileHeight)
                {
                    case TILEHEIGHT.ONE:
                        prefabType = TileContainer.Instance.RampOne;
                        break;
                    case TILEHEIGHT.TWO:
                        prefabType = TileContainer.Instance.RampTwo;
                        break;
                    default:
                        break;
                }
                break;
            case TILETYPE.STEEP_RAMP:
                switch (tileHeight)
                {
                    case TILEHEIGHT.ONE:
                        prefabType = TileContainer.Instance.SteepRampOne;
                        break;
                    case TILEHEIGHT.TWO:
                        prefabType = TileContainer.Instance.SteepRampTwo;
                        break;
                    default:
                        break;
                }
                break;
            case TILETYPE.CORNER:
                switch (tileHeight)
                {
                    case TILEHEIGHT.ONE:
                        prefabType = TileContainer.Instance.CornerOne;
                        break;
                    case TILEHEIGHT.TWO:
                        prefabType = TileContainer.Instance.CornerTwo;
                        break;
                    default:
                        break;
                }
                break;
            case TILETYPE.STEEP_CORNER:
                switch (tileHeight)
                {
                    case TILEHEIGHT.ONE:
                        prefabType = TileContainer.Instance.SteepCornerOne;
                        break;
                    case TILEHEIGHT.TWO:
                        prefabType = TileContainer.Instance.SteepCornerTwo;
                        break;
                    default:
                        break;
                }
                break;
            default:
                break;
        }
        return prefabType;
    }

    void AddWall()
    {
        var selectedObjects = Selection.gameObjects;

        for (int i = 0; i < selectedObjects.Length; ++i)
        {
            GameObject wall = GetSpecifiedWall();
            if (wall != null)
            {
                wall.transform.parent = selectedObjects[i].transform;
                SetWallPosition(ref wall);
            }
            else
            {
                foreach (Transform child in selectedObjects[i].transform)
                {
                    if (child.name.Contains("Wall"))
                    {
                        DestroyImmediate(child.gameObject);
                        // TODO: Create link between adjacent tiles where the wall was blocking

                    }
                }
            }

            // Remove link between tiles if there is a wall inbetween them
            for (int j = 0; j < selectedObjects[i].GetComponent<Tile>().AdjacentTiles.Count; ++j)
            {
                switch (wallPosition)
                {
                    case WALLPOSITION.ZPOSITIVE:
                        if (selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].transform.position.z > selectedObjects[i].transform.position.z)
                        {
                            // Remove the selected tile from the adjacent tile's list before removing the adjacent tile from the selected tile's list
                            for (int k = 0; k < selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles.Count; ++k)
                            {
                                if (selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles[k] == selectedObjects[i].GetComponent<Tile>())
                                {
                                    selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles.RemoveAt(k);
                                }
                            }
                            selectedObjects[i].GetComponent<Tile>().AdjacentTiles.RemoveAt(j);
                        }
                        break;
                    case WALLPOSITION.ZNEGATIVE:
                        if (selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].transform.position.z < selectedObjects[i].transform.position.z)
                        {
                            // Remove the selected tile from the adjacent tile's list before removing the adjacent tile from the selected tile's list
                            for (int k = 0; k < selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles.Count; ++k)
                            {
                                if (selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles[k] == selectedObjects[i].GetComponent<Tile>())
                                {
                                    selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles.RemoveAt(k);
                                }
                            }
                            selectedObjects[i].GetComponent<Tile>().AdjacentTiles.RemoveAt(j);
                        }
                        break;
                    case WALLPOSITION.XPOSITIVE:
                        if (selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].transform.position.x > selectedObjects[i].transform.position.x)
                        {
                            // Remove the selected tile from the adjacent tile's list before removing the adjacent tile from the selected tile's list
                            for (int k = 0; k < selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles.Count; ++k)
                            {
                                if (selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles[k] == selectedObjects[i].GetComponent<Tile>())
                                {
                                    selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles.RemoveAt(k);
                                }
                            }
                            selectedObjects[i].GetComponent<Tile>().AdjacentTiles.RemoveAt(j);
                        }
                        break;
                    case WALLPOSITION.XNEGATIVE:
                        if (selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].transform.position.x < selectedObjects[i].transform.position.x)
                        {
                            // Remove the selected tile from the adjacent tile's list before removing the adjacent tile from the selected tile's list
                            for (int k = 0; k < selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles.Count; ++k)
                            {
                                if (selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles[k] == selectedObjects[i].GetComponent<Tile>())
                                {
                                    selectedObjects[i].GetComponent<Tile>().AdjacentTiles[j].GetComponent<Tile>().AdjacentTiles.RemoveAt(k);
                                }
                            }
                            selectedObjects[i].GetComponent<Tile>().AdjacentTiles.RemoveAt(j);
                        }
                        break;
                    default:
                        break;
                }
            }

        }
    }

    GameObject GetSpecifiedWall()
    {
        GameObject obj;
        switch (wallType)
        {
            case WALLTYPE.NONE:
                return null;
            case WALLTYPE.HALF_WALL:
                obj = (GameObject)PrefabUtility.InstantiatePrefab(TileContainer.Instance.HalfWall);
                return obj;
            case WALLTYPE.WALL:
                obj = (GameObject)PrefabUtility.InstantiatePrefab(TileContainer.Instance.Wall);
                return obj;
            case WALLTYPE.EDGE_HALF_WALL:
                obj = (GameObject)PrefabUtility.InstantiatePrefab(TileContainer.Instance.EdgeHalfWall);
                return obj;
            case WALLTYPE.EDGE_WALL:
                obj = (GameObject)PrefabUtility.InstantiatePrefab(TileContainer.Instance.EdgeWall);
                return obj;
            default:
                return null;
        }
    }

    void SetWallPosition(ref GameObject _wallObject)
    {
        Vector3 pos = Vector3.zero;
        Vector3 rot = Vector3.zero;
        pos.y = 1.0f;
        switch (wallPosition)
        {
            case WALLPOSITION.ZPOSITIVE:
                rot.y = -90.0f;
                break;
            case WALLPOSITION.ZNEGATIVE:
                rot.y = -90.0f;
                pos.z = -1.0f;
                break;
            case WALLPOSITION.XPOSITIVE:
                pos.x = 1.0f;
                break;
            case WALLPOSITION.XNEGATIVE:
                break;
            default:
                break;
        }
        _wallObject.transform.localPosition = pos;
        _wallObject.transform.localEulerAngles = rot;
    }

    void LinkTiles()
    {
        var selectedObjects = Selection.gameObjects;

        for (int i = 0; i < selectedObjects.Length; ++i)
        {
            for (int j = 0; j < selectedObjects.Length; ++j)
            {
                if (i != j)
                {
                    bool alreadyAdjacent = false;
                    // Check current adjacent tiles
                    foreach (Tile adjacentTile in selectedObjects[i].GetComponent<Tile>().AdjacentTiles)
                    {
                        if (adjacentTile == selectedObjects[j])
                        {
                            alreadyAdjacent = true;
                            break;
                        }
                    }

                    if (!alreadyAdjacent)
                    {
                        // Check adjacent
                        if (selectedObjects[j].transform.position.x + 1 == selectedObjects[i].transform.position.x ||
                            selectedObjects[j].transform.position.x - 1 == selectedObjects[i].transform.position.x ||
                            selectedObjects[j].transform.position.z + 1 == selectedObjects[i].transform.position.z ||
                            selectedObjects[j].transform.position.z - 1 == selectedObjects[i].transform.position.z)
                        {
                            selectedObjects[i].GetComponent<Tile>().AdjacentTiles.Add(selectedObjects[j].GetComponent<Tile>());
                        }
                    }
                }
            }
        }
    }

    void RotateClockWise()
    {
        var selectedObjects = Selection.gameObjects;

        for (int i = 0; i < selectedObjects.Length; ++i)
        {
            Vector3 pos = selectedObjects[i].transform.localPosition;
            Vector3 rot = selectedObjects[i].transform.localEulerAngles;

            rot.y += 90.0f;
            if (rot.y >= 360.0f)
                rot.y = 0.0f;

            if (rot.y == 0.0f)
            {
                pos.z += 1.0f;
            }
            if (rot.y == 90.0f)
            {
                pos.x += 1.0f;
            }
            else if (rot.y == 180.0f)
            {
                pos.z -= 1.0f;
            }
            else if (rot.y == 270.0f)
            {
                pos.x -= 1.0f;
            }
            selectedObjects[i].transform.localPosition = pos;
            selectedObjects[i].transform.localEulerAngles = rot;
        }
    }

    void RotateCounterClockWise()
    {
        var selectedObjects = Selection.gameObjects;

        for (int i = 0; i < selectedObjects.Length; ++i)
        {
            Vector3 pos = selectedObjects[i].transform.localPosition;
            Vector3 rot = selectedObjects[i].transform.localEulerAngles;

            rot.y -= 90.0f;
            if (rot.y < 0.0f)
                rot.y = 270.0f;

            if (rot.y == 0.0f)
            {
                pos.x -= 1.0f;
            }
            if (rot.y == 90.0f)
            {
                pos.z += 1.0f;
            }
            else if (rot.y == 180.0f)
            {
                pos.x += 1.0f;
            }
            else if (rot.y == 270.0f)
            {
                pos.z -= 1.0f;
            }

            selectedObjects[i].transform.localPosition = pos;
            selectedObjects[i].transform.localEulerAngles = rot;
        }
    }
}
